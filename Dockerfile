FROM node:16.0-alpine AS BUILD_IMAGE

WORKDIR /app

COPY . .

RUN npm install

RUN npm run build



FROM httpd:latest

ENV UID 1000
ENV GID 1000

COPY --from=BUILD_IMAGE /app/dist /usr/local/apache2/htdocs

import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
// import Home from '../views/Home.vue';
import Countdown from '../views/Countdown.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    meta: {
      title: 'TN\'Event'
    },
    component: Countdown
  },
  {
    path: '/about',
    name: 'About',
    meta: {
      title: 'TN\'Event - À propos'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/asso/humanitn',
    name: 'Humanitn',
    meta: {
      title: 'TN\'Event - Humani\'TN'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Humanitn.vue')
  },
  {
    path: '/asso/actionenfance',
    name: 'Action Enfance',
    meta: {
      title: 'TN\'Event - Action Enfance'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ActionEnfance.vue')
  },
  {
    path: '/equipe',
    name: 'L\'équipe',
    meta: {
      title: 'TN\'Event - L\'équipe'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Equipe.vue')
  },
  {
    path: '/partenaires',
    name: 'Les partenaires',
    meta: {
      title: 'TN\'Event - Nos partenaires'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Partenaires.vue')
  },
  {
    path: '/countdown',
    name: 'Countdown',
    props: {
      default: true,
      donations: 0,
      eventFinished: false
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Countdown.vue')
  },
  {
    path: '/components',
    name: 'Components',
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Components.vue')
  },
  {
    path: '/streamers',
    name: 'Streamers',
    meta: {
      title: 'TN\'Event - Les streamers'
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Streamers.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
    meta: {
      title: 'TN\'Event - Nous contacter'
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Contact.vue')
  },
  {
    path: '/tombola',
    name: 'Tombola',
    meta: {
      title: 'TN\'Event - Tombola'
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Tombola.vue')
  },
  {
    path: '/donations',
    name: 'Donations',
    meta: {
      title: 'TN\'Event - Donations'
    },
    props: {
      default: true,
      donations: 0
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Donations.vue')
  },
  {
    path: '/statistiques',
    name: 'Statistiques',
    meta: {
      title: 'TN\'Event - Statistiques'
    },
    props: {
      default: true,
      donations: 0,
      viewers: 0,
      visitors: 0
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Statistiques.vue')
  },
  {
    path: '/stream/:channel',
    name: 'Stream',
    meta: {
      title: 'TN\'Event - Voir'
    },
    component: () => import(/* webpackChunkName: "countdown" */ '../views/Stream.vue')
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // e.g., if we have `/some/deep/nested/route` and `/some`, `/deep`, and `/nested` have titles,
  // `/nested`'s will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) {
    if (typeof (nearestWithTitle.meta.title) === 'string') {
      document.title = nearestWithTitle.meta.title;
    }
  } else if (previousNearestWithMeta) {
    if (typeof (previousNearestWithMeta.meta.title) === 'string') {
      document.title = previousNearestWithMeta.meta.title;
    }
  }

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => (el.parentNode as Node).removeChild(el));

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  (nearestWithMeta.meta.metaTags as []).map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  }).forEach(tag => document.head.appendChild(tag)); // Add the meta tags to the document head.

  next();
});

export default router;

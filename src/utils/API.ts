import { BACKEND, BACKEND_WS } from './../params';

export interface Streamer {
    name: string,
    description: string,
    twitchId: string,
    isLive: boolean,
    img: string,
    viewerCount: number
}

export interface Member {
  name: string,
  description: string,
  role: string,
  img: string,
  id: number
}

export interface Sponsor {
  name: string,
  description: string,
  link: string,
  img: string,
  id: number
}

export async function getMembers () : Promise<Array<Member>> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/team').then((res) => {
      return res.json();
    }).then((data) => {
      resolve(data);
    }).catch((e) => {
      reject(e);
    });
  });
}

export async function getDonationLink () : Promise<string> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/donationlink').then((res) => {
      return res.json();
    }).then((data) => {
      resolve(data.link);
    }).catch((e) => {
      reject(e);
    });
  });
}

export async function getDonationAmount () : Promise<number> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/donations').then((res) => {
      return res.json();
    }).then((data) => {
      resolve(data.amount);
    }).catch((e) => {
      reject(e);
    });
  });
}

export async function getSponsors () : Promise<Array<Sponsor>> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/sponsors').then((res) => {
      return res.json();
    }).then((data) => {
      resolve(data);
    }).catch((e) => {
      reject(e);
    });
  });
}

export async function getStreamers () : Promise<Array<Streamer>> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/streamers').then((res) => {
      return res.json();
    }).then((data) => {
      resolve(data);
    }).catch((e) => {
      reject(e);
    });
  });
}

export async function isEventStarted () : Promise<boolean> {
  const startdate = await startDate();
  const today = Date.now();
  return Promise.resolve(today >= startdate);
}

export async function isEventFinished () : Promise<boolean> {
  const enddate = await endDate();
  const today = Date.now();
  return Promise.resolve(today >= enddate);
}

export async function timeBeforeStart () : Promise<number> {
  const startdate = await startDate();
  const today = Date.now();
  return Promise.resolve(startdate - today);
}

export async function timeBeforeEnd () : Promise<number> {
  const enddate = await endDate();
  const today = Date.now();
  return Promise.resolve(enddate - today);
}

export function startDate () : Promise<number> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/startdate').then((res) => {
      return res.json();
    }).then((data) => {
      const date = Date.parse(data.date as string);
      resolve(date);
    }).catch((e) => {
      reject(e);
    });
  });
}

export function endDate () : Promise<number> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/startdate').then((res) => {
      return res.json();
    }).then((data) => {
      const date = Date.parse(data.endDate as string);
      resolve(date);
    }).catch((e) => {
      reject(e);
    });
  });
}

export function getDates () : Promise<{startDate: number, endDate: number, isStarted: boolean, isFinished: boolean}> {
  return new Promise((resolve, reject) => {
    fetch(BACKEND + '/startdate').then((res) => {
      return res.json();
    }).then((data) => {
      const startDate = Date.parse(data.date as string);
      const endDate = Date.parse(data.endDate as string);
      const today = Date.now();
      const isStarted = today >= startDate;
      const isFinished = today > endDate;
      resolve({
        startDate,
        endDate,
        isStarted,
        isFinished
      });
    }).catch((e) => {
      reject(e);
    });
  });
}

export const updateSocket = new WebSocket(BACKEND_WS + '/ws');

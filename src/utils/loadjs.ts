export async function loadScript (path: string) : Promise<void> {
  return new Promise<void>((resolve, reject) => {
    let done = false;
    const scr = document.createElement('script');

    const handleLoad = function () {
      if (!done) {
        done = true;
        resolve();
      }
    };

    const handleReadyStateChange = function () {
      let state;

      if (!done) {
        // eslint-disable-next-line
        state = (scr as any).readyState;
        if (state === 'complete') {
          handleLoad();
        }
      }
    };

    const handleError = function () {
      if (!done) {
        done = true;
        reject(new Error('error loading image'));
      }
    };

    scr.onload = handleLoad;
    // eslint-disable-next-line
    (scr as any).onreadystatechange = handleReadyStateChange;
    scr.onerror = handleError;
    scr.src = path;
    document.body.appendChild(scr);
  });
}
